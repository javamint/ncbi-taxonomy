#/bin/bash

gem build ncbi_taxonomy.gemspec
gem install ncbi-taxonomy-0.1.1.gem
taxonomy "Escherichia coli str. SE11"
taxonomy "Rhodobacterales bacterium Alpha_01_strain"
