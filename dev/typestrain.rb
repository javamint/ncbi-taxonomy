#!/usr/bin/env ruby

require_relative '../lib/ncbi_taxonomy'

t = Taxonomy.new
t.memory
strains = Array.new

ts = t.q "SELECT tax_id, name_txt FROM names WHERE name_class='type material'"
ts.each do |x|
	species = t.q "SELECT name_txt FROM names WHERE tax_id = #{x[0]} AND name_class='scientific name'"
	st = t.q "SELECT name_txt FROM names WHERE tax_id = (SELECT tax_id FROM nodes WHERE parent_tax_id=#{x[0]})"
	st.each do |s|
		strains << [species[0], x, s[0]]
		puts "#{[species[0], x, s[0]]}"
	end
end
