require 'rubygems'

Gem::Specification.new do |s|
  s.name = 'ncbi-taxonomy'
  s.version = '0.2.11'
  s.date = '2017-03-30'
  s.summary = "NCBI Taxonomy search using local repository"
  s.description = "This gem supports essential functions for fast access of NCBI Taxonomy database."
  s.authors = ["Seok-Won Kim"]
  s.email = 'javamint@gmail.com'
  s.bindir = 'bin'
  s.executables = [
	  'ncbi_taxonomy'
  ]
  s.files = [
	  'bin/ncbi_taxonomy',
	  'lib/ncbi_taxonomy_update.rb',
	  'lib/ncbi_taxonomy.rb'
  ]
  s.homepage = 'https://gitlab.com/javamint/ncbi-taxonomy'
  s.license = 'Nonstandard'
  s.add_runtime_dependency 'sqlite3', '~> 1.0', '>= 1.0.0'
end
