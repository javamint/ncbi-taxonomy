# Fast search tool for the NCBI Taxonomy database
## System requirements
General unix & linux machines

(I'm sorry but this tool is not supported in MS Windows.)

[SQLite 3](https://www.sqlite.org)


## Installation
    gem install ncbi-taxonomy
    
## Command line usage
### Basic help
    ncbi_taxonomy

### Download the NCBI Taxonomy database
    ncbi_taxonomy update

### Taxonomy search by name
    ncbi_taxonomy name "Escherichia coli SE11"

    ncbi_taxonomy name Escherichia

### Taxonomy search by taxonid
    ncbi_taxonomy id 409438

### Include taxonomic rank in the result
    ncbi_taxonomy name -r "Escherichia coli SE11"

    ncbi_taxonomy id -r 409438
    
### Read from the file
    ncbi_taxonomy name -i list.txt

## Ruby API usage
~~~ruby
require 'ncbi_taxonomy'

taxonomy = Taxonomy.new
# If you want to load whole database into memory
taxonomy.memory

taxonomy.get_allrank_by_name "Escherichia coli SE11"

taxonomy.get_fixedrank_by_name "Acidibacter ferrireducens"

taxonomy.get_allrank_by_id 100
~~~
