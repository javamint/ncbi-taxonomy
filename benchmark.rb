#!/usr/bin/env ruby
require 'ncbi_taxonomy'
require 'benchmark'

t = Taxonomy.new
arr = Array.new(10000) {|x| x+10000 }

Benchmark.bm(10) do |x|
 	x.report("general:")   { arr.each { |i| t.get_allrank_by_id i } }
	x.report("parallel 1:") { t.mget_allrank_by_id arr }
 	x.report("inmemory 1:")   { t.memory; arr.each { |i| t.get_allrank_by_id i } }
	x.report("inmemory 2:")   { arr.each { |i| t.get_allrank_by_id i } }
	x.report("parallel 2:") { t.mget_allrank_by_id arr }
end